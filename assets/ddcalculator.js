$(document).ready(function(){
  $('#startDate').datetimepicker({
    firstDayOfWeek: 6,
    minTime: '09:00',
    theme: 'dark',
    format: 'Y-m-d H:i',
    value: 'Y-m-d H:i',
    disabledWeekDays: [0,6],
    allowTimes: ['09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00', '17:00']
  });
  $('.btn-calculate').click(function(e){
    var form = $('form.calculate-form').serialize();
    $.ajax({
      method: 'post',
      url: 'calc.php',
      data: form,
      success: function(ret){
        var data = JSON.parse(ret);
        if (data['error']) {
          $('#duelist').append('<li class="list-group-item list-group-item-error">[ERROR] ' + data['error'] + ' </li>');
        } else {
          $('#duelist').append('<li class="list-group-item list-group-item-success">' + data['start_date'] + ' (' + data['turnaround'] + ' hours), due date: ' + data['end_date'] + '</li>');
        }
      }
    });
  });

});

